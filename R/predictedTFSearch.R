#' A SearchPredictedTFs Function
#' This function helps you to search predicted transcriptionfactors for a single gene or set of gene sets for selected salmonid species and related fish genomes.
#' @param geneList
#' valid gene id: For EntrezG, the valid input format is GeneID (e.g., 106586456). However, ID:GeneID (e.g., gene47690:106586456) is also acceptable.
#' @param mySpecies Supported species are: Atlantic salmon, Coho salmon, Rainbow trout, Arctic char and Chinook salmon
#' Species: Atlantic salmon, Oncorhynchus kisutch, Oncorhynchus mykiss, Oncorhynchus tshawytscha,Salvelinus alpinus
#' @param topTFs top number of sorted motifs for network graph. Maximum 10.
#' enriched motifs will be sorted by descending based on their pvalues.
#' @param dbType type of biological sequence that motifs were identified
#' For example, from upstream dna sequence (DNA-seq), the assay for transposase-accessible chromatin (ATAC-seq)
#' @keywords salmotifdb
#' @export
#' @examples
#' SearchPredictedTFs(geneList = geneList, mySpecies = mySpecies)

SearchPredictedTFs <- function(geneList,mySpecies,...){
  .datatable.aware = TRUE
  
  tableNamesATACseq <- copy(tableNamesATACseq)
  setDT(tableNamesATACseq)
  
  tableNamesPromoter <- copy(tableNamesPromoter)
  setDT(tableNamesPromoter)
  
  tableName <- head(tableNamesPromoter[commonName == mySpecies , tableName],1)
  dbName <- head(tableNamesPromoter[commonName == mySpecies , dbName],1)
  
  geneList <- MassageList(geneList)  # MASSAGE PROVIDED LIST
  resTable <- PredictTFsSQL(dbName, tableName, geneList)
  
  if (!is.null(resTable)) {
    colnames(resTable) <- c("Gene ID", "Gene start", "Gene end", "Gene strand", "Chromosome", "Gene name", "Product","TF","E-value","Bitscore","Predicted as TF","CDD ID","Accession ID","TF superfamily","CD name")
    resultList <- list(predictedTFs=resTable)
    
  }else{
    print("No result found.")
  }
}
