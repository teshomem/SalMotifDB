#' A InputPosition FUNCTION
#' This function processes strings input for position based motif enrichment
#' @param tableName tableName
#' @param coordinate coordinates
#' @keywords input, position
#' @export
#' @examples
#' This function is called by other function
#' InputPosition()

InputPosition <- function(dbName, tableName, coordinate, totalMotifHit, ...) {
  
  if (tableName == "choose") {
    return(NULL)
  }
  chromosome <-
    unlist(strsplit(coordinate, split = ':'))[1]
  
  coordinate <-
    unlist(strsplit(coordinate, split = ':'))[2]
  
  p1 <- unlist(strsplit(coordinate, split = '-'))[1]
  p2 <- unlist(strsplit(coordinate, split = '-'))[2]
  
  resTable <-
    PositionSQL(dbName, tableName, chromosome, p1, p2, totalMotifHit)
}
