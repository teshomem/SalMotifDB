#' A DBConnect FUNCTION
#' This function helps to onnect to the databases
#' @param dbname dbname
#' @param host host
#' @param username username
#' @param password password
#' @keywords dbconnect
#' @export
#' @examples
#' This function is called by other function
#' DBConnect()
DbConnect <- function(dbname, host, username, password, ...) {
  dbConnect(
    drv = RMySQL::MySQL(),
    dbname = dbname,
    host = host,
    username = username,
    password = password
  )
}
