---
title: "SalMotifDB quick start guide"
author: 
  - name: Teshome Dagne Mulugeta
    affiliation:
    - &NMBU Norwegian University of Life Sciences (NMBU)
    - &Elixir Elixir-Norway
    - &CIGENE Center for Integrative Genetics (CIGENE)
    email: teshome.mulugeta@nmbu.no
output: 
  BiocStyle::html_document:
    self_contained: yes
    toc: true
    toc_float: true
    toc_depth: 2
    code_folding: show
date: "`r doc_date()`"
package: "`r pkg_ver('SalMotifDB')`"
 
vignette: >
  %\VignetteIndexEntry{SalMotifDB quick start guide}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}

---

               
```{r load necessary libraries, echo = FALSE, include=FALSE}
library(data.table)
library(SalMotifDB)
# devtools::install_github("haozhu233/kableExtra")
# devtools::install_github("Bioconductor/BiocStyle")
library(knitr, quietly = T, verbose = F)
library(kableExtra, quietly = T, verbose = F)
options(knitr.table.format = "html")
library(BiocStyle)
```

```{r, echo = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
```

```{r code, echo = FALSE}
code <- function(...) {
    cat(paste(..., sep = "\n"))
}
date = "`r doc_date()`"
pkg = "`r pkg_ver('BiocStyle')`"
```

```{r global_options, echo=FALSE}
# short=TRUE #if short==TRUE, do not echo code chunks
# debug=FALSE
# knitr::opts_chunk$set(echo=!short, warning=debug, message=debug, error=FALSE,
#                cache.path = "cache/",
#                fig.path = "figures/")
```

# Introduction

Recently developed genome resources in Salmonid fish provides tools for studying the genomics underlying a wide range of properties including life history trait variation in the wild, economically important traits in aquaculture and the evolutionary consequences of whole genome duplications. Although genome assemblies now exist for a number of salmonid species, the lack of regulatory annotations are holding back our mechanistic understanding of how genetic variation in non-coding regulatory regions affect gene expression and the downstream phenotypic effects.

Eukaryotic regulatory regions are characterized based a set of discovered transcription factor binding sites (TFBSs), which can be represented as sequence patterns with various degree of degeneracy.

This [SalMotifDB](https://gitlab.com/teshomem/SalMotifDB) package and its associated [web interface](https://salmobase.org/apps/SalMotifDB/) is designed to be a computational tool for the analysis of transcription factors (TFs) and their cis-regulatory binding sites in five salmonid genomes. SalMotifDB integrates TF-binding site information for non-redundant DNA patterns (motifs) assembled from a large number of metazoan motif databases.

So far this package contains a set of integrated functions. All functions access a public database so you need to have an internet access to benefit from this tool. Alternatively you can use the [SalMotifDB web interface](https://salmobase.org/apps/SalMotifDB/).

# SalMotifDB functions
The following tutorial demonstrate how to operate on the current version of SalMotifDB database using available functions in [SalMotifDB](https://gitlab.com/teshomem/SalMotifDB) package, the interpretation of the results and some associated methods defined for these functions.
 
# Data: lipidGenes
To explore the basic data manipulation verbs of SalMotifDB, we'll use the `lipidGenes` dataset shipped with `SalMotifDB` R package `r data( 'lipidGenes', package='SalMotifDB')`. This dataset contains `r nrow(lipidGenes)` genes grouped into different KEGG pathway. The dataset is obtained from [Life‐stage‐associated remodelling of lipid metabolism regulation in Atlantic salmon](https://doi.org/10.1111/mec.14533).

## EnrichMotif

This enrichment function allows you to input a list of genes (e.g. differentially expressed genes) and identify motifs that match the promoters of these genes more often than expected by chance. The tool gives details about enrichment p-values (using the hypergeometric distribution), as well as details about all individual motif matches to promoters of genes in the list.
For more details, please see the help page for as `?EnrichMotif()`.

### Load the example dataset.

We will extract 17 genesets for "Fatty acid elongation, Biosynthesis of unsaturated fatty acids, and Fatty acid metabolism" KEGG pathway

```{r get examåle dataset, echo=TRUE, eval=TRUE}
data( 'lipidGenes', package='SalMotifDB')
fam <- as.data.table(lipidGenes[KEGG_pathway_name == 'Fatty acid elongation, Biosynthesis of unsaturated fatty acids, Fatty acid metabolism', gene_id])
fam$V1
```

### Run EnrichMotif function
Using the gene list we prepared above, run EnrichMotif function for Atlantic salmon species and for motifs predicted in upstream promoter sequences. 

```{r EnrichMotif, echo=TRUE, eval=TRUE}
resultList <- EnrichMotif(myFile=fam,mySpecies="Atlantic salmon",topTFs=10,dbType="DNA-seq",RepeatsMasked="Yes", Conserved = "Yes")
```

The result contains 5 elements

* enrichedMotifs: Most enriched motif for the genesets

* associatedGenes: Target genes for each motif

* networkEdges: Selected genes network edges via shared TFs

* networkNodes: Selected genes network nodes via shared TFs

* resTableBed: result table in bed format


Let's walk through the objects one by one.

<br>

**A) enrichedMotifs**

```{r enriched moitfs results, echo=TRUE, eval=TRUE}
enrichedmotifs <- resultList$enrichedMotifs
knitr::kable(enrichedmotifs[1:10], format="html")  %>% kable_styling(bootstrap_options = c("striped"), font_size = 10, full_width = T, position = "center")  %>% scroll_box(width = "80%", height = "200px")
```

This table (scroll the table to see all rows and columns) gives details about enrichment p-values (using the hypergeometric distribution), as well as details about all individual motif matches to promoters of genes in the list. Let's go through the columns in detail.

**Motif ID:** motif id by source database. Each Motif ID made up of three parts delimited by underscore. 

* For example, Six-3_M01358_TRANSFAC. 
  * Six-3: the TF name that binds to the binding site
  * M01358: motif id from the source database
  * TRANSFAC: source database

**TF:** The transcription factor name that binds to the moitf.

**Central TF:** To reduce motif redundancy, we clustered our motif collections from different sources. We first clustered motifs within each database and then clustered the central motifs (i.e. the motif with the highest similarity to other motifs in the cluster calculated by matrix-clustering) of these database-specific clusters across databases. Each cluster represented by one non-redundant central motif. This column shows the representative TF for each cluster.

**Central motif ID:** The motif ID for the Central TF in the SalMotifDB.

**Database source:** The source database that the motif is obtained. 

**Occurrence in geneset:** The motif occurrence in your test geneset.

**Occurrence in genome:** The motif occurrence in the genome.

**P-value:**  Hypergeometric distribution p-value.

<br>

**B) associatedGenes**

```{r associated genes results, echo=TRUE, eval=TRUE}
associatedgenes <- resultList$associatedGenes
knitr::kable(associatedgenes[1:10, -c("Gene strand", "Motif strand")], format="html")  %>% kable_styling(bootstrap_options = c("striped"), font_size = 10, full_width = T, position = "center")  %>% scroll_box(width = "80%", height = "200px")
```

This table (scroll the table to see all rows and columns) provides details about all individual motif matches to the promoters of genes in the list. The first five columns are about each gene and are obtained from NCBI database annotation. *Motif ID*, *TF*, *Central TF* and *Central motif ID* are explained above. We will explain some of the columns.

**Score:** the log-odds scores using log base 2 computed by the FIMO tool used to scan motifs.

**Distance:** the motif distance from transcription start site

**Start:** Motif start location in the genome

**Stop:** Motif stop location in the genome

**Matched sequence:** Actual matched sequence in the promoter sequence

**P-value:** statistical threshold used by FIMO (<0.0001)

<br>

**C) networkEdges and networkNodes**

```{r network, echo=TRUE}
edges <-  resultList$networkEdges
nodes <-  resultList$networkNodes

setDT(edges)
edges[, size := .N, by='tf']

nodes <- as.data.table(unique(left_join(nodes,edges[,.(tf,size,count)], by=c("label"="tf"))))
nodes[is.na(size), size := 0]

nodes[ , shape := ifelse(size == 0, "circle", no="triangle")]
nodes[ , color := ifelse(size == 0, "#5CFFFF", no="#F25FD0")]
nodes[size != 0, value := count]

library(visNetwork)
visNetwork(nodes=nodes, edges=edges, physics = FALSE, width = "90%") %>% visIgraphLayout() %>%
visOptions(highlightNearest = TRUE, nodesIdSelection = TRUE) %>% visNodes(scaling = list(label = list(enabled = T))) %>% visNetwork::visHierarchicalLayout(parentCentralization = FALSE, enabled = FALSE)
```

We provide a method to visualize the relationship between genes and their associated TFs. The above network visualization prepared from the top 10 enriched motifs sorted by p-value. The network diagram is interactive so that you can click on a gene (blue circle) or a TF (red triangle) to see its relationship with other nodes.  

## MotifSearchPosition

The position based search tool allows you to specify a genomic region of interest and retrieve details about all motif matches to promoters of genes located in that region.

### Run MotifSearchPosition function
Run MotifSearchPosition function for Atlantic salmon species and for motifs predicted for sequences that weren't repeat masked. Let's find motifs between 1 and 1000000 base pairs in chromosome ssa01
```{r MotifSearchPosition, echo=TRUE, eval=TRUE}
resultList <- MotifSearchPosition(coordinate="ssa01:1-1000000",mySpecies="Atlantic salmon",topTFs=10,dbType="DNA-seq",RepeatsMasked="Yes", Conserved = "Yes")
```

The result contains only one element: identifiedMotifs.

```{r MotifSearchPosition results, echo=TRUE, eval=TRUE}
identifiedMotifs <- resultList$identifiedMotifs
knitr::kable(identifiedMotifs[1:10, -c("Gene strand", "Motif strand")], format="html")  %>% kable_styling(bootstrap_options = c("striped"), font_size = 10, full_width = T, position = "center")  %>% scroll_box(width = "80%")

```


## MotifSearchGene

This MotifSearchGene function allows you to input a list of genes and identify motifs that match the promoters of these genes more often than expected by chance. The tool gives details about enrichment p-values (using the hypergeometric distribution), as well as details about all individual motif matches to promoters of genes in the list. 

For more search criteria, please check the usage of motif enrichment analysis function as `?MotifSearchGene()`.

### Load the example dataset.

We will extract 3 genes for Fatty acid metabolism KEGG pathway

```{r MotifSearchGene get examåle dataset, echo=TRUE, eval=TRUE}
data( 'lipidGenes', package='SalMotifDB')
fam <- lipidGenes[KEGG_pathway_name == 'Fatty acid metabolism', gene_id]
fam
```

### Run MotifSearchGene function

Run MotifSearchGene function for Atlantic salmon species and for motifs predicted for sequences that weren't repeat masked

```{r MotifSearchGene, echo=TRUE, eval=TRUE}
resultList <- MotifSearchGene(fam,mySpecies="Atlantic salmon",topTFs=10,dbType="DNA-seq",RepeatsMasked="Yes", Conserved = "Yes")
```

**MotifSearchGene enrichedMotifs**

```{r MotifSearchGene enriched moitfs results, echo=TRUE, eval=TRUE}
enrichedMotifs <- resultList$enrichedMotifs
knitr::kable(enrichedMotifs[1:10,], format="html")  %>% kable_styling(bootstrap_options = c("striped"), font_size = 10, full_width = T, position = "center")  %>% scroll_box(width = "80%", height = "200px")
```

This table gives details about enrichment p-values (using the hypergeometric distribution), as well as details about all individual motif matches to promoters of genes in the list. The columns are explained under enrichedMotifs function above.

```{r MotifSearchGene associated genes results, echo=TRUE, eval=TRUE}
associatedgenes <- resultList$associatedGenes
knitr::kable(associatedgenes[1:10, -c("Gene strand", "Motif strand")], format="html")  %>% kable_styling(bootstrap_options = c("striped"), font_size = 10, full_width = T, position = "center")  %>% scroll_box(width = "80%", height = "200px")
```

This gives details about all individual motif matches to promoters of genes in the list. The first five columns are about each gene and are obtained from gene annotation file in the NCBI database. The columns are explained under enrichedMotifs function above.

**networkEdges and networkNodes**
  
```{r MotifSearchGene network, echo=TRUE}
edges <-  resultList$networkEdges
nodes <-  resultList$networkNodes

setDT(edges)
edges[, size := .N, by='tf']

nodes <- as.data.table(unique(left_join(nodes,edges[,.(tf,size,count)], by=c("label"="tf"))))
nodes[is.na(size), size := 0]

# nodes_d3[ , shape := ifelse(size == 0, "dot", no="triangle")]
nodes[ , shape := ifelse(size == 0, "circle", no="triangle")]
nodes[ , color := ifelse(size == 0, "#5CFFFF", no="#F25FD0")]
nodes[size != 0, value := count]

library(visNetwork)
visNetwork(nodes=nodes, edges=edges, physics = FALSE, width = "90%") %>% visIgraphLayout() %>%
visOptions(highlightNearest = TRUE, nodesIdSelection = TRUE) %>% visNodes(scaling = list(label = list(enabled = T))) %>% visNetwork::visHierarchicalLayout(parentCentralization = FALSE, enabled = FALSE)
    
```

The network diagram is explained under enrichedMotifs function above.
  
## SearchPredictedTFs

This SearchPredictedTFs function allows you to search predicted transcription factors for a single gene or set of genes for selected salmonid species. The TFs are predicted salmonid orthologs with information on BLAST E-value score and shared NCBI conserved domain database (CDD).

For more search criteria, please check the usage of SearchPredictedTFs analysis function as `?SearchPredictedTFs()`.

### Load the example dataset.

We will use all 1421 genes for lipid metabolism genes in our dataset.

```{r SearchPredictedTFs get examåle dataset, echo=TRUE, eval=TRUE}
data( 'lipidGenes', package='SalMotifDB')
fam <- lipidGenes$gene_id
length(fam)
```

### Run SearchPredictedTFs function

Run SearchPredictedTFs function for Atlantic salmon species to check if a gene is predicted as TF.

```{r SearchPredictedTFs, echo=TRUE, eval=TRUE}
resultList <- SearchPredictedTFs(fam,mySpecies="Atlantic salmon")

predictedTFs <- resultList$predictedTFs
knitr::kable(predictedTFs[,-c("Gene strand")], format="html")  %>% kable_styling(bootstrap_options = c("striped"), font_size = 10, full_width = T, position = "center")  %>% scroll_box(width = "80%", height = "200px")
```

We have got  `r nrow(predictedTFs)` genes predicted as TF. As you can see in the table, some important TFs in lipid metabolism such as SREBF1, SREBF2, LXR, RXRA and PPA are predicted. 
