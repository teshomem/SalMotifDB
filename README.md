# SalMotifDB

`SalMotifDB` is an R package that provides access to transcription factor binding sites (TFBSs) database for salmonids.

## Install/update SalMotifDB

You can track (and contribute to) development of `SalMotifDB` at `'https://gitlab.com/teshomem/salmotifdb` 

Dependencies:

1. Install the release version of `devtools` from CRAN with `install.packages("devtools")`.

2. Install the release version of `AnnotationDbi` from bioconductor as follows:

`source("http://bioconductor.org/biocLite.R")`
`biocLite("AnnotationDbi")`

3. Install the release version of `RMySQL` from CRAN with `install.packages("RMySQQL")`. You might need mysql client 

 * deb: libmariadbclient-dev | libmariadb-client-lgpl-dev (Debian, Ubuntu)
 * rpm: mariadb-connector-c-devel | mariadb-devel | mysql-devel (Fedora, CentOS, RHEL)
 * csw: mysql56_dev (Solaris)
 * brew: mariadb-connector-c (OSX)

Finally, install `SalMotifDB` from GitLab using:
```R
library(devtools)
devtools::install_gitlab("teshomem/salmotifdb",build = TRUE,  upgrade = 'never' )
library(SalMotifDB)

#Check SalMotifDB vignette
browseVignettes(package = "SalMotifDB")

```

To uninstall `SalMotifDB`

`detach("package:SalMotifDB")`

`remove.packages("SalMotifDB")`


[Tutorial](https://v2.salmobase.org/datasets/2019-TMulugeta-SalMotifDB/doc/SalMotifDB.html)

---
